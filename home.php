<?php
require "config/helper.php";
require "config/url.class.php";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<?php include "components/heads.php"; ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
	<link rel="stylesheet" href="./assets/css/swiper.css">
</head>

<body>
	<?php include "components/navbar.php"; ?>
	<div class="flex flex-col items-center justify-center min-h-screen">
		<h1 class="text-5xl mr-3 font-bold">CAIRO</h1>
		<h1 class="text-5xl font-bold">CODES</h1>
		<p class="text-md text-center">Códigos Avançados do Início ao Fim.</p>
	</div>
	<script src="./assets/js/script.js"></script>
	<script src="https://unpkg.com/flowbite@1.4.1/dist/flowbite.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>
	<script>
		var swiper = new Swiper(".swiper_banners", {
			slidesPerView: 1.1,
			centeredSlides: true,
			spaceBetween: 30,
			loop: true,
			freeMode: true,
			pagination: {
				el: ".swiper-pagination",
				clickable: true,
			},
		});
	</script>
</body>

</html>