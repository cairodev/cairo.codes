<?php
$URI = new URI();
?>
<title>Cairo Codes - cairo.codes</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="" name="keywords">
<meta content="" name="description">
<meta name="author" content="Cairo Felipe Developer">

<meta property="og:title" content="" />
<meta property="og:url" content="https://www.ittnet.net.br/" />
<meta property="og:image" content="https://www.ittnet.net.br/assets/img/imgog.jpg" />
<meta property="og:description" content="" />

<link rel="stylesheet" href="./assets/css/style.css">
<link href="<?php echo $URI->base("/assets/img/favicon.png"); ?>" rel="icon">
<link href="<?php echo $URI->base("/assets/img/favicon.png"); ?>" rel="apple-touch-icon">
<script src="https://cdn.tailwindcss.com"></script>
<script>
	tailwind.config = {
		theme: {
			extend: {
				colors: {
					color1: '',
					color2: '',
					color3: '',
				}
			}
		}
	}
</script>