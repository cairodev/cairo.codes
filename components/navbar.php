<header class="w-full">
  <nav class="border-gray-200 bg-gradient-to-r from-color1 to-color2 pt-4 pb-4 md:pb-0">
    <div class="flex flex-wrap items-center justify-between max-w-6xl px-4 mx-auto">
      <a href="#">
      </a>
      <div class="flex items-center lg:order-2 space-x-2">
        <div>
          <a href="" class="text-white text-lg">
            <i class="bi bi-whatsapp"></i>
          </a>
        </div>
        <div id="theme_toggler" x-data="{ show: true }">
          <i @click="show = !show" :class="{'hidden': !show, 'block':show }" class="bi bi-moon-stars-fill"></i>
          <i @click="show = !show" :class="{'block': !show, 'hidden':show }" class="bi bi-brightness-high-fill"></i>
        </div>
        </button>
      </div>
    </div>
  </nav>
</header>